# dbot
dbot is a tool that automates data collection in dating apps. Currently only supports [tinder](https://tinder.com/). This project is in a very early stage of development. 

## Dependencies
- Docker & Docker-compose
- VNC Client
  - Linux: [novnc](https://aur.archlinux.org/packages/novnc/), found in AUR

## Installation
Installation has not yet been tested on another machine. Hopefully this will also work in Windows and Mac. 

**NOTE:** This docker container will run with `--privileged` flag, raising some security concerns listed [here](https://www.trendmicro.com/en_us/research/19/l/why-running-a-privileged-container-in-docker-is-a-bad-idea.html#:~:text=Privileged%20containers%20in%20Docker%20are,not%20accessible%20in%20ordinary%20containers.). Shortly summarized: anyone can potentially watch you browse the web. Login to tinder with your account at **your own risk**.

### Linux
Download this repo with:
```shell
git clone https://gitlab.com/keklas/dbot.git
cd dbot
```
Next, make sure that your docker client is running. On linux you can start it with:
```shell
sudo systemctl start docker
```
Build the docker image with the following command. This will take 5-15min depending on your system specs and internet connection speed. 
```shell
docker-compose build
```
Everything is done once you see: `successfully built`. Run the built image with:
```shell
docker-compose up
```
You will need to use `1234` as the password when you connect to the system with your VNC client. Again, everything should be working if you see something like `The VNC desktop is:` or `PORT=5600`.

Start your VNC session with:
```shell
novnc
```

Navigate to <a href="http://localhost:6080/vnc.html" target="_blank">http://localhost:6080/vnc.html</a> Now you essentially have a linux machine in your browser that has all the necessary components installed. This is where the magic happens. Time to start collecting data!


**How to end sessions:**

You can end your `novnc` or `docker` sessions by pressing CTRL+C inside the terminal


### Windows
Instructions will be made later, if this even works.



## Usage
Login to the main control interface at <a href="http://localhost:6080/vnc.html" target="_blank">http://localhost:6080/vnc.html</a>. The magic happens here. The password is the one you set earlier in `VNC_SERVER_PASSWORD=<password>`

More usage instructions will be added once the enviroment is working as it should
