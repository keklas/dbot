from selenium import webdriver
from selenium import __version__ as selenium_version
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException
import sys, os, traceback, subprocess

class Menu:
    def __init__(self, driver):
        self.login_state = False
        self.driver = driver.loadDriver()
        self.config = driver.getConfig()
        self.headless = self.config["launch"]["headless"]
        self.verbose = self.config["launch"]["verbose"]
        self.debug = self.config["launch"]["debug"]

        # Accept Cookies
        self.driver_click('//*[@id="content"]/div/div[2]/div/div/div[1]/button')

        self.options_main_logged_out = {
            "0" : self.quit,
            "1" : self.run_login,
            "2" : self.run_debug
        }
        self.options_main_logged_out_debug = {
            "0" : self.quit,
            "1" : self.run_login,
            "2" : self.run_debug
        }
        self.options_main_logged_in = {
            "0" : self.quit,
            "1" : self.run_main,
            "2" : self.run_main,
            "3" : self.run_main,
            "4" : self.run_test_features
        }
        self.options_login = {
            "0" : self.run_exit_from_login,
            "1" : self.run_login_with_phone,
            "2" : self.run_login_with_other,
            "3" : self.run_login_with_other
        }
        self.options_test_features = {
            "0" : self.run_main,
            "1" : self.run_main,
            "2" : self.run_main,
            "3" : self.run_main,
            "4" : self.run_main,
            "5" : self.run_main,
            "6" : self.run_main,
            "7" : self.run_main,
            "8" : self.run_main,
            "9" : self.run_main,
            "10" : self.run_main,
            "11" : self.run_main,
            "12" : self.run_main
        }

    # ----------| DEBUG MESSAGES |----------
    def vprint(self,message):
        if self.verbose:
            print(message)
    # ----------| MENU PRINTS |----------
    def menu_main(self):
        #os.system("clear")
        if self.login_state:
            print("""
---| Main Menu |--- Logged in: {0}
0: Exit / Logout
1: Extract Data
2: View Local Data
3: Train AI
4: Test Features
""".format(self.login_state))
        else:
            if self.debug:
                print("""
    ---| Main Menu |--- Logged in: {0}
    0: Exit
    1: Login
    2: Debug
    """.format(self.login_state))
            else:
                print("""
    ---| Main Menu |--- Logged in: {0}
    0: Exit
    1: Login
    2: View Local Data
    """.format(self.login_state))

    def menu_login_options(self):
        print("""
0: Back
1: Phone
2: Google
3: Facebook
""")
    def menu_test_features(self):
        os.system("clear")
        print("""
0:  Back
1:  Show Messages
2:  Show Matches
3:  Train AI
4:  Train AI
5:  Test Features
6:  Test Features
7:  Test Features
8:  Test Features
9:  Test Features
10: Test Features
11: Test Features
12: Test Features
""")
    # ----------| MENU LOGIC |----------
    def run_main(self):
        while True:
            self.menu_main()
            choice = input("Enter an option: ")
            if self.login_state:
                action = self.options_main_logged_in.get(choice)
            else:
                if self.debug:
                    action = self.options_main_logged_out_debug.get(choice)
                else:
                    action = self.options_main_logged_out.get(choice)

            if action:
                action()

    def getDriverVersion(self):
        cmd = "./bin/chromedriver --version | cut -d' ' -f2"
        return subprocess.check_output(cmd)
    def run_debug(self):
        # test anything here

        print("Current Path: \t" + str(os.path.dirname(__file__)))
        print("Dimensions: \t" + str(self.driver.get_window_size()))
        print("Selenium version: \t" + str(selenium_version))
        print("Browser version: \t" + str(self.driver.capabilities['browserVersion']))
        ####print("Driver version: \t" + str(self.getDriverVersion().decode("utf-8")))
        print("Headless: \t" + str(self.headless))
        print("Verbose: \t" + str(self.verbose))
        print("Debug: \t\t" + str(self.debug))
        self.driver_screenshot("test.png")


        # end of testing area
        self.run_main()
    def run_test_features(self):
        while True:
            self.menu_test_features()
            choice = input("Enter an option: ")
            action = self.options_test_features.get(choice)
            if action:
                action()
    def run_login(self):
        self.driver_click("//*[@id='content']/div/div[1]/div/main/div[1]/div/div/header/div[1]/div[2]/div/button")
        while True:
            self.menu_login_options()
            choice = input("Enter a login option: ")
            action = self.options_login.get(choice)
            if action:
                action()
    def run_login_with_phone(self):
        self.driver_click('//*[@id="modal-manager"]/div/div/div[1]/div/div[3]/span/div[3]/button')
        while True:
            try:
                number = input("Enter phone number: ")
                test = int(number)

                break
            except Exception:
                print("Invalid format try again\n")

    def run_login_with_other(self):
        self.driver_click('//*[@id="modal-manager"]/div/div/div[1]/div/div[3]/span/div[1]/div/button')
        while True:
            if(input("Press Enter after successful login") == ""): break

        # ADD A CHECK IF SOME BUTTONS EXISTS, OTHERWISE LOGIN FAILED
        self.login_state = True
        self.run_main()
    def quit(self):
        exit_option = input("Confirm exit? Requires relogin. y/N: ")
        if exit_option == "y" or exit_option == "Y":
            self.login_state = False
            sys.exit(0)
        else:
            self.run_main()
    def run_exit_from_login(self):
        self.driver_click('//*[@id="modal-manager"]/div/div/div[2]/button')
        self.run_main()

    # ----------| Driver Actions |----------
    def driver_click(self,path):
        try:
            self.driver.find_element_by_xpath(path).click()
        except:
            print(traceback.print_exc())
            self.run_main()
    def driver_element_visible(self,path):
        try:
            self.driver.find_element_by_xpath(path).click()
            return true
        except:
            return false
    def driver_screenshot(self,output):
        self.driver.save_screenshot(output)
if __name__ == '__main__':
    Menu().run_main(self)


