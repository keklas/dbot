#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Just to make sure colors are always present
force_color_prompt=yes

# Some env variables
export EDITOR="nvim"

# Terminal prefix, works well with fira-code font
PS1='\[\e[33;1m\]\u@\h \[\e[34m\]==> \[\e[31m\]\W |-\[\e[0m\] '

# Source aliases, in a separate file
[[ -f ~/.bash_aliases ]] && . ~/.bash_aliases

cat << EOF
dbot - Tinder data collection utility

Browser opens automatically on workspace 2.
EOF
