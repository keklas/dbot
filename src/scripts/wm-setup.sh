#!/bin/bash
# This script is executed inside docker
cp /usr/dbot/config/dialog/dialogrc /etc/dialogrc && echo "Dialog config copied"
cp -r /usr/dbot/config/i3blocks /home/vnc/.config/ && echo "Statusbar config copied"
cp -r /usr/dbot/config/bash/ /home/vnc/ && echo "Bash files copied"
cp -r /usr/dbot/config/X11/.Xresources /home/vnc/ && echo "X11 files copied
