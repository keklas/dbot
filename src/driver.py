from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException
import toml, os, sys

class Driver:
    def __init__(self):
        path_current = os.path.dirname(__file__)
        path_abs = os.path.join(path_current, "config.toml")

        # Config settings
        self.config = toml.load(path_abs)
        self.verbose = self.config["launch"]["verbose"]
        self.fullscreen = self.config["launch"]["fullscreen"]
        self.debug = self.config["launch"]["debug"]
        self.window_dimension = self.config["browser"]["window_dimension"]


        self.vprint("Verbose: {0}\nDebug: {1}".format(self.verbose,self.debug))
    def loadDriver(self):
        option = webdriver.ChromeOptions()
        #option.binary_location = self.config["browser"]["path"]

        # Removes info bars from the browser
        option.add_experimental_option("useAutomationExtension", False)
        option.add_experimental_option("excludeSwitches", ["enable-automation"])

        # If headless
        if self.config["launch"]["headless"] == True:
            self.vprint("Launching without browser")
            option.add_argument("headless")
            option.add_argument("--window-size=%s" % self.window_dimension)
        # If not headless
        else:
            self.vprint("Launching with browser")
            if self.fullscreen:
                # NOTE DO NOT TRY TO CHANGE DRIVER SIZE IF FULLSCREEN = TRUE,
                # DRIVER WILL CRASH
                option.add_argument("--kiosk")

        self.vprint(os.system("pwd"))
        self.vprint(os.system("ls -al"))
        self.vprint(os.system("ls ./bin/ -al"))
        browser = webdriver.Chrome(
            executable_path=self.config["driver"]["path"],
            options=option)
        print(browser.capabilities['browserVersion'])
        browser.get("https://tinder.com/app/recs")
        browser.implicitly_wait(self.config["driver"]["max_loadtime"])
        return browser
    def getPath(self,parent,file):
        return os.path.abspath(os.path.join(os.path.dirname(__file__), parent, file))
    def getConfig(self):
        return self.config
    def vprint(self,message):
        if self.verbose:
            print(message)

