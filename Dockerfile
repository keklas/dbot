FROM ubuntu:18.04
ARG DEBIAN_FRONTEND=noninteractive
WORKDIR /usr/dbot/

# Initialize package installation
RUN apt-get update; apt-get clean
RUN apt-get install -y apt-utils dialog locales

# Necessary env variable
ENV TERM=linux

# Install Python
RUN apt-get install -y --no-install-recommends python3 python3-pip

# Install Browser and Driver
RUN apt-get install -y --no-install-recommends chromium-browser chromium-chromedriver

# Install VNC Components
RUN apt-get install -y x11vnc xvfb wget wmctrl

#RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - 
#RUN echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list
#RUN apt-get update && apt-get -y install google-chrome-stable

# Install Window Manager and additional tools
RUN apt-get install -y i3 i3blocks conky-all fonts-firacode fonts-font-awesome bash-completion sudo locales locales-all neovim

# Add new non-root user
RUN useradd -s /bin/bash -g root -G sudo vnc
RUN mkdir -p /home/vnc && chown vnc:root /home/vnc

# Define Passwords
RUN echo "root:1234" | chpasswd && echo "vnc:1234" | chpasswd

# Install project dependencies
ADD ./src/requirements.txt /usr/dbot/
RUN pip3 install -r requirements.txt

# Post-installation
ENV PATH=/usr/dbot/scripts:${PATH} 
USER vnc

