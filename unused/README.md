# Tinder Data Collector

## Features / Ideas
- Ability to extract and save profile data
  - Names, age, bio, pictures
  - You can train an AI on the bio text data
- Race and ethnicity prediction
  - Tensorflow + Keras + OpenCV
## Installation

- selenium
  - chromedriver
  - for other browsers, check the guide [here](https://selenium-python.readthedocs.io/installation.html)
- standalone selenium server
  - java 8
  - check another guide [here](https://motorscript.com/install-selenium-arch-linux/)



